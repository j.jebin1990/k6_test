import http from 'k6/http';

export const options = {
  discardResponseBodies: true,
  scenarios: {
    products: {
      executor: 'constant-vus',
      exec: 'products',
      vus: 50,
      duration: '30s',
    },
    reviews: {
      executor: 'per-vu-iterations',
      exec: 'reviews',
      vus: 50,
      iterations: 100,
      startTime: '30s',
      maxDuration: '1m',
    },
  },
};

export function products() {
  http.get('http://34.125.43.222/api/v1/products', {
    tags: { my_custom_tag: 'products' },
  });
}

export function reviews() {
  http.get('http://34.125.43.222/api/v1/products/1/reviews', { tags: { my_custom_tag: 'reviews' } });
}

